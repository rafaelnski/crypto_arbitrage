
Crypto arbitrage is web app which allow user to live preview price diff between crypto exchanges.

to start project run:

1. python manage.py migrate
2. create .env file, you can use boilerplate .env_template.txt 
3. add coinbase / binance api keys 
4. run command: python manage.py update_meta_data
5. run:
- docker-compose up
- celery worker -A crypto_arbitrage.celery_app --pool=solo -l info
- celery beat --app=crypto_arbitrage.celery_app --loglevel=info
6. python manage.py runserver
7. use username: developer, password: developer to login to app.

<img alt="alt_text" width="100%" src="project.png" />
