from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import LogoutView
from .views import DashboardView, SingInView
from .api import ChartInitialData

api_urls = [
    path("chart-initial-data/", ChartInitialData.as_view(), name="chart-data"),
]

urlpatterns = [
    path("", DashboardView.as_view(), name="dashboard"),
    path("accounts/login/", SingInView.as_view(), name="sing-in"),
    path("accounts/logout/", LogoutView.as_view(), name="sing-out"),
    path("api/", include((api_urls, "api"))),
    path("admin/", admin.site.urls),
]
