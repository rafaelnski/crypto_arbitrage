from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from crypto_arbitrage.models import Crypto


class ChartInitialData(APIView):
    def get(self, request, *args, **kwargs):
        dataset, dates = Crypto.objects.get_dataset()
        return Response(
            data={"dataset": dataset, "dates": dates}, status=status.HTTP_200_OK
        )
