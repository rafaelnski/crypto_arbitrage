import logging
from datetime import timedelta
from crypto_arbitrage.models import Crypto, PriceChange
from crypto_arbitrage.utils import get_arbitrage
from celery.task import periodic_task
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

logger = logging.getLogger("handler")


@periodic_task(
    name="crypto_arbitrage.get_crypto_data",
    serializer="json",
    ignore_result=True,
    run_every=timedelta(seconds=5),
)
def get_crypto_data():
    for crypto in list(
        Crypto.objects.filter(is_active=True).values_list("name", flat=True)
    ):
        try:
            arbitrages, crypto_data = get_arbitrage(crypto)
            PriceChange.objects.bulk_create(arbitrages)
            layer = get_channel_layer()
            async_to_sync(layer.group_send)(
                "crypto_quotes",
                {"crypto_data": crypto_data, "type": "crypto_quotes.data"},
            )
        except Exception as e:
            logger.error(e)
            continue
