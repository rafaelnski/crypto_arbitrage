from django.contrib.auth.views import LoginView
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import MultipleObjectMixin
from crypto_arbitrage.models import PriceChange


class SingInView(LoginView):
    template_name = "crypto_arbitrage/sing_in.html"


class DashboardView(LoginRequiredMixin, TemplateView, MultipleObjectMixin):
    template_name = "crypto_arbitrage/dashboard.html"
    paginate_by = 50

    def get_context_data(self, **kwargs):
        arbitrages = PriceChange.objects.filter(tracker="binance|coin_base")
        paginator, page, arbitrages, is_paginated = self.paginate_queryset(
            arbitrages, self.paginate_by
        )
        context = {
            "paginator": paginator,
            "page_obj": page,
            "is_paginated": is_paginated,
            "arbitrages": arbitrages,
        }
        return context
