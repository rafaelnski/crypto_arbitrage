from django.contrib import admin
from .models import Crypto, CryptoQuote, Exchange, PriceChange, AccessToken


admin.site.register(
    [Crypto, CryptoQuote, Exchange, PriceChange, AccessToken], admin.ModelAdmin
)
