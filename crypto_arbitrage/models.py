from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone


class Exchange(models.Model):
    name = models.CharField(max_length=255)
    std_name = models.CharField(max_length=255)
    url = models.URLField()

    @property
    def app_access_token(self):
        return AccessToken.objects.get(exchange=self, is_app_token=True)

    @property
    def client(self):
        from crypto_arbitrage.client import CLIENT_MAPPER

        return CLIENT_MAPPER[self.std_name](
            api_key=self.app_access_token.api_key,
            api_secret=self.app_access_token.secret_key,
        )

    def __str__(self):
        return f"{self.std_name}"


class CryptoManager(models.Manager):
    def get_dataset(self):
        qs = self.all()
        dataset = []
        for price_type in ["bid", "ask"]:
            for crypto in qs:
                dataset.append(
                    {
                        "name": f"{crypto.__str__()}|{price_type}",
                        "data": list(
                            crypto.cryptoquote_set.all().values_list(
                                price_type, flat=True
                            )[:30]
                        ),
                    }
                )
        qs = crypto.cryptoquote_set.all()
        qs = qs.extra(select={"datetime_date": "DATE(datetime)"})
        dates = list(qs.values_list("datetime_date", flat=True)[:30])
        return dataset, dates


class Crypto(models.Model):
    name = models.CharField(max_length=255)
    symbol = models.CharField(max_length=12)
    exchange = models.ForeignKey("crypto_arbitrage.Exchange", on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    objects = CryptoManager()

    def __str__(self):
        return f"{self.symbol}|{self.exchange}"


class CryptoQuote(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    crypto = models.ForeignKey("crypto_arbitrage.Crypto", on_delete=models.CASCADE)
    bid = models.FloatField()
    ask = models.FloatField()

    class Meta:
        ordering = ["-datetime"]


class PriceChange(models.Model):
    """price difference on exchanges"""

    datetime = models.DateTimeField(auto_now_add=True)
    bid_diff = models.FloatField()
    ask_diff = models.FloatField()
    potential_profit = models.FloatField()
    tracker = models.CharField(max_length=36)

    class Meta:
        ordering = ["-datetime"]


class AccessToken(models.Model):
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="%(app_label)s_%(class)s",
    )
    exchange = models.ForeignKey("crypto_arbitrage.Exchange", on_delete=models.CASCADE)
    is_app_token = models.BooleanField(default=False)
    api_key = models.CharField(max_length=255)
    secret_key = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    expires = models.DateTimeField(null=True)

    def is_valid(self):
        return not self.is_expired()

    def is_expired(self):
        if not self.expires:
            return False
        return timezone.now() >= self.expires

    def __str__(self):
        return self.api_key
