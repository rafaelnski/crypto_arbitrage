import random
from datetime import datetime
from django.core.management.base import BaseCommand
from django.db import transaction
from crypto_arbitrage.models import Crypto, Exchange, CryptoQuote


class Command(BaseCommand):
    help = "Update meta data"

    def handle(self, *args, **options):
        with transaction.atomic():
            coin_base, _ = Exchange.objects.get_or_create(
                name="Coin base", url="coinbase.com", std_name="coin_base"
            )
            coin_base_btc, _ = Crypto.objects.get_or_create(
                name="Bitcoin", exchange=coin_base, symbol="BTC-EUR"
            )
            CryptoQuote.objects.all().delete()
            for x in range(30):
                ask = random.randint(30000, 30100)
                bid = random.randint(30000, 30100)
                CryptoQuote.objects.create(
                    datetime=datetime(
                        2020,
                        1,
                        1,
                        hour=random.randint(1, 23),
                        minute=random.randint(1, 59),
                    ),
                    crypto=coin_base_btc,
                    ask=ask,
                    bid=bid,
                )
        print("Success")
