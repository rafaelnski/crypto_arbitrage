import os
from django.core.management.base import BaseCommand
from django.db import transaction
from django.contrib.auth import get_user_model
from crypto_arbitrage.models import AccessToken, Exchange, Crypto


class Command(BaseCommand):
    help = "Update meta data"

    def handle(self, *args, **options):
        with transaction.atomic():
            # SUPER USER
            User = get_user_model()
            user, _ = User.objects.get_or_create(
                username="developer", is_staff=True, is_active=True, is_superuser=True
            )
            user.set_password("developer")
            user.save()
            # COINBASE
            coin_base, _ = Exchange.objects.get_or_create(
                name="Coin base", url="coinbase.com", std_name="coin_base"
            )
            coin_base_btc, _ = Crypto.objects.get_or_create(
                name="Bitcoin", exchange=coin_base, symbol="BTC-EUR"
            )
            coin_base_access_token, _ = AccessToken.objects.get_or_create(
                is_app_token=True,
                exchange=coin_base,
                api_key=os.environ.get("COINBASE_API_KEY"),
                secret_key=os.environ.get("COINBASE_SECRET_KEY"),
            )
            # BINANCE
            binance, _ = Exchange.objects.get_or_create(
                name="Binance", url="binance.com", std_name="binance"
            )
            binance_btc, _ = Crypto.objects.get_or_create(
                name="Bitcoin", exchange=binance, symbol="BTC-EUR"
            )
            binance_access_token, _ = AccessToken.objects.get_or_create(
                is_app_token=True,
                exchange=binance,
                api_key=os.environ.get("BINANCE_API_KEY"),
                secret_key=os.environ.get("BINANCE_SECRET_KEY"),
            )
