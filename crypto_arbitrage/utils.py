from crypto_arbitrage.models import Exchange, PriceChange
from crypto_arbitrage.client import ClientAdapter


def get_arbitrage(crypto):
    market_data = {}
    crypto_data = []
    for exchange in Exchange.objects.all():
        client = ClientAdapter(exchange.client, exchange.std_name)
        tick_prices = client.get_prices(crypto)
        market_data[exchange.std_name] = {
            "bid": tick_prices["bid"],
            "ask": tick_prices["ask"],
        }
        crypto_data.append(tick_prices)
    arbitrages = []
    for i_key, i_value in market_data.items():
        for j_key, j_value in market_data.items():
            if i_key != j_key:
                bid_diff = None
                ask_diff = None
                if i_value["bid"] != j_value["bid"]:
                    bid_diff = float(i_value["bid"]) - float(j_value["bid"])
                if i_value["ask"] != j_value["ask"]:
                    ask_diff = float(i_value["ask"]) - float(j_value["ask"])
                potential_profit = -(float(i_value["bid"]) - float(j_value["bid"]))
                if bid_diff or ask_diff:
                    arbitrage = PriceChange(
                        tracker=f"{i_key}|{j_key}",
                        bid_diff=bid_diff,
                        ask_diff=ask_diff,
                        potential_profit=potential_profit,
                    )
                    arbitrages.append(arbitrage)
    return arbitrages, crypto_data
