from channels.generic.websocket import JsonWebsocketConsumer
from asgiref.sync import async_to_sync


class CryptoQuoteConsumer(JsonWebsocketConsumer):
    def connect(self):
        async_to_sync(self.channel_layer.group_add)("crypto_quotes", self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            "crypto_quotes", self.channel_name
        )
        self.close()

    def receive_json(self, content, **kwargs):
        self.send_json(content)

    def crypto_quotes_data(self, event):
        self.send_json(
            {
                "type": "crypto_quotes.data",
                "crypto_data": event["crypto_data"],
            }
        )
