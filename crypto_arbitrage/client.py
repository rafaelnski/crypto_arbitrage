from coinbase.wallet.client import Client as CoinBaseClient
from binance.client import Client as BinanceClient
from crypto_arbitrage.models import Crypto, CryptoQuote

CLIENT_MAPPER = {"binance": BinanceClient, "coin_base": CoinBaseClient}


class ClientAdapter:
    def __init__(self, client, exchange):
        self.client = client
        self.exchange = exchange

    def _get_ask_price(self, crypto_symbol):
        if self.exchange == "coin_base":
            response = self.client.get_sell_price(currency_pair=crypto_symbol)
            ask = response["amount"]
        else:
            response = self.client.get_orderbook_ticker(symbol=crypto_symbol)
            ask = response["askPrice"]
        return ask

    def _get_bid_price(self, crypto_symbol):
        if self.exchange == "coin_base":
            response = self.client.get_buy_price(currency_pair=crypto_symbol)
            bid = response["amount"]
        else:
            response = self.client.get_orderbook_ticker(symbol=crypto_symbol)
            bid = response["bidPrice"]
        return bid

    def get_prices(self, crypto_name):
        crypto = Crypto.objects.only("symbol").get(
            name=crypto_name, exchange__std_name=self.exchange
        )
        bid, ask = self._get_bid_price(crypto.symbol), self._get_ask_price(
            crypto.symbol
        )
        quote = CryptoQuote.objects.create(crypto=crypto, bid=bid, ask=ask)
        return {
            "type": "crypto_quotes.data",
            "bid": bid,
            "ask": ask,
            "crypto": crypto.__str__(),
            "datetime": quote.datetime.strftime("%Y-%m-%d %H:%M:%S"),
        }
