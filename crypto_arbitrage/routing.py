from django.urls import re_path
from crypto_arbitrage.consumers import CryptoQuoteConsumer

websocket_urlpatterns = [
    re_path(r"ws/crypto-quotes/$", CryptoQuoteConsumer.as_asgi()),
]
