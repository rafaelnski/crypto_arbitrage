const path = require('path');
const webpack = require('webpack')
const VueLoaderPlugin  = require('vue-loader/lib/plugin')


module.exports = {
    node: false,
  entry: {
  	main: './src/js/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'static/chart'),
    filename: '[name].js'
  },
    module: {
    rules: [
      // ... other rules
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        exclude: '/node_modules/'
      },
           {
        test: /\.css$/i,
        exclude: '/node_modules/',
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.s(c|a)ss$/,
                exclude: '/node_modules/',
        use: [
          'vue-style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            // Requires sass-loader@^7.0.0
            options: {
              implementation: require('sass'),
              indentedSyntax: true // optional
            },
            // Requires sass-loader@^8.0.0
            options: {
              implementation: require('sass'),
              sassOptions: {
                indentedSyntax: true // optional
              },
            },
          },
        ],
      },
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
    new webpack.ProvidePlugin({
    $: 'jquery',
    jquery: 'jquery',
    'window.jQuery': 'jquery',
    jQuery: 'jquery'
  })
  ],
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      '@': path.resolve('src'),
    }
  },
};
